-- BEGIN;
-- --
-- -- Create model motion_apporoved
-- --
-- CREATE TABLE `motion_apporoved` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `motion_id` integer NOT NULL, `action_name` varchar(64) NOT NULL, `approver` varchar(64) NOT NULL, `memo` longtext NULL);
-- COMMIT;

-- BEGIN;
-- --
-- -- Add field is_confirmed to motion_apporoved
-- --
-- ALTER TABLE `motion_apporoved` ADD COLUMN `is_confirmed` bool DEFAULT b'1' NOT NULL;
-- ALTER TABLE `motion_apporoved` ALTER COLUMN `is_confirmed` DROP DEFAULT;
-- COMMIT;

BEGIN;
--
-- Add field is_deleted to motions
--
ALTER TABLE `motions` ADD COLUMN `is_deleted` bool DEFAULT b'0' NOT NULL;
ALTER TABLE `motions` ALTER COLUMN `is_deleted` DROP DEFAULT;
COMMIT;