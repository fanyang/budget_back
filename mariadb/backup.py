import os
import sys
import datetime

# docker exec budgetback_budget_back_mariadb_1 /usr/bin/mysqldump -u root --password=123456 budget_back > backup_budget.sql
def back_sql(container_name = 'budgetback_budget_back_mariadb_1', password = '123456', user = 'root'):
  now = datetime.datetime.now()
  print( dir(now) )
  now_str = now.strftime('%Y_%m_%d_%H_%M_%S')
  backup_file_name = 'back_up_{now_str}.sql'.format(now_str = now_str)
  print(backup_file_name)

  out_put_cmd = '''
    docker exec {container_name} /usr/bin/mysqldump -u {user} --password={password} budget_back > backup_budget.sql
  '''\
  .format(
      container_name = container_name,
      password       = password,
      user           = user
    )

  with os.popen(out_put_cmd) as cmd:
    pass

  print( out_put_cmd )

def filter_args(*args):
  for row in args:
    pass
  
# back_sql()
if __name__ == '__main__':
  print(sys.argv)
  print('start')
