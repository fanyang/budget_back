import Sequelize from 'sequelize'

import config from '../config'

import structrues from './db_structure'

const { database } = config;

const Model = Sequelize.Model;

console.log(database);

const sequelize = new Sequelize(database.db_name, database.name, database.password, {
  host: database.host,
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  logging: false
});


const models = {};

// init table models
(() => {

  structrues.forEach((item) => {
    models[ item.configs.tableName ] = sequelize.define(item.configs.tableName, item.keys, item.configs);
  });

})();

export const excute_sql = async (sql) => {
  let res = await sequelize.query(sql, null, { raw: false });
  return res[0];
}
/*
  model_name: String
  unique_keys: [String|Array]
*/
export const update_or_create_model = async (model_name, unique_keys = 'id', value) => {
  unique_keys = unique_keys.slice && unique_keys.splice ? unique_keys : [unique_keys];
  let _model = models[model_name];
  
  let filter_condition = {};
  unique_keys.forEach(row => {
    filter_condition[row] = value[row];
  });

  let result = await _model.findAll({where: filter_condition});

  if(result.length > 1){
    throw Error({msg: 'unique_key is not right'})
  }
  if(!result.length){
    result = await _model.create(value);
    return result;
  }
  result = result[0];
  Object.assign(result, value);
  result.save();
  return result;
}

export const get_lastest = async (model_name, query_keys = 'id') => {
  let _model = models[model_name];
  let result = await _model.findAll({order: [[query_keys, 'DESC']], limit: 1});
  return result[0];
}

export default models;