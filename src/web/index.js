import Express from 'express'

import models from '../db_manage/models'

import {
  excute_sql
} from '../db_manage/models'

const app = Express();
app.use(Express.json());

const VERSION = '1';
app.all('*',function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild');
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');

  if (req.method == 'OPTIONS') {
    res.sendStatus(200); /让options请求快速返回/
  } else {
    next();
  }
});

app.all('/web/get_motions.do', async (req, res) => {
  let form = req.body;
  let step = 10, {page = 10, status = '', head_block_num = ''} = form;

  let filters = `order by motion_id desc limit ${ (page - 1) * step }, 10`

  if(status == 1){
    filters = `where section = 0 and is_deleted=0 and approve_end_block_num >= ${head_block_num} ${filters} `;
  }else if(status == 2){
    filters = `where section = 1 and is_deleted=0 and approve_end_block_num >= ${head_block_num} ${ filters } `
  }else if(status == 3){
    filters = `where section = 1 and is_deleted=0 and approve_end_block_num < ${head_block_num} ${filters} `;
  }

  let query_res = await excute_sql(`select * from motions ${filters}`);
  query_res.forEach(i => i.status == status);
  res.json({
    more: query_res.length == 10 ? true : false,
    page,
    rows: query_res
  });
});

app.all('/web/get_memo_by_motion.do', async (req, res) => {
  let form = req.body;
  let { motion_id } = form;

  let result = await excute_sql(`select * from motion_apporoved where motion_id = ${motion_id}`)
  res.json({
    rows: result
  })
});

app.get('/web/get_total_reward.do', async (req, resp) => {
  let res = await excute_sql('select quantity from motions');
  let total = 0;
  res.forEach(item => {
    total += parseFloat(item.quantity);
  });
  total = total.toFixed(4);
  resp.json({
    total
  })
});

const port = process.env.NODE_PORT || 3010;
app.listen(port, () => {
  console.log(`server port is ${port}`)
});